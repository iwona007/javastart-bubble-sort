# javastart-bubble-sort

Napisz program, którego zadaniem jest posortowanie rosnąco liczb w tablicy 
za pomocą algorytmu sortowania bąbelkowego.

Do posortowania tablicy wykorzystaj klasę anonimową, która będzie implementowała 
zdefiniowany przez Ciebie interfejs Sortable z metodą sort() przyjmującą jako parametr tablicę liczb.

Tablica może być zdefiniowana na sztywno w kodzie programu.

Wykorzystanie klasy random. 
