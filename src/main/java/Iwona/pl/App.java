package Iwona.pl;


public class App {
    public static void main(String[] args) {

        RandomArray randomArray = new RandomArray();
        randomArray.sortable.sort(randomArray.tabRandom);
        System.out.println();
        randomArray.printArray(randomArray.tabRandom);

        System.out.println();
        NormalArray normalArray = new NormalArray();
        normalArray.printTabSort(normalArray.tabSort);
        normalArray.sortable.sort(normalArray.tabSort);
        normalArray.printTabSort(normalArray.tabSort);
    }
}
