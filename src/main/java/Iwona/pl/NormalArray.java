package Iwona.pl;

public class NormalArray {

    int[] tabSort = {8, 12, 5, 1, 0, 15, 6, 4, 9, 10};

    Sortable sortable = new Sortable() {
        @Override
        public void sort(int[] tab) {
            for (int przejscie = 1; przejscie < tabSort.length; przejscie++) {
                for (int obecnyElement = 0; obecnyElement < tabSort.length - przejscie; obecnyElement++) {
                    if (tabSort[obecnyElement] > tabSort[obecnyElement + 1]) {
                        int temp = tabSort[obecnyElement];
                        tabSort[obecnyElement] = tabSort[obecnyElement + 1];
                        tabSort[obecnyElement + 1] = temp;
                    }
                }
            }
        }
    };

    public void beforeSort(){
        for (int element : tabSort) {
            System.out.print(element + "\t");
        }
        System.out.println();
    }
    public void printTabSort(int[] tabSort) {
        for (int element : tabSort) {
            System.out.print(element + "\t");
        }
        System.out.println();
    }
}
