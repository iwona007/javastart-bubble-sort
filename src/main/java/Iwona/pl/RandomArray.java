package Iwona.pl;

import java.util.Random;

public class RandomArray {
    Random random = new Random();
    final int[] tabRandom = new int[10];
    private void createRandomArray() {
        for (int i = 0; i < tabRandom.length; i++) {
            tabRandom[i] = random.nextInt(50);
            System.out.print(tabRandom[i] + "\t");
        }
    }

    Sortable sortable = new Sortable() {
        @Override
        public void sort(int[] tabSort) {
            createRandomArray();
            for (int i = 0; i < tabRandom.length; i++) {
                for (int j = 1; j < tabRandom.length - i; j++) {
                    if (tabRandom[j - 1] > tabRandom[j]) {
                        int temp = tabRandom[j];
                        tabRandom[j] = tabRandom[j - 1];
                        tabRandom[j - 1] = temp;
                    }
                }
            }
        }
    };

    public void printArray(int[] tabSort) {
        for (int element : tabSort) {
            System.out.print(element + "\t");
        }
        System.out.println();
    }
}
